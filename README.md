# IDS 721 Mini Proj 10 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-10/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-10/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 10 - Rust Serverless Transformer Endpoint**

## Goal
* Dockerize Hugging Face Rust transformer
* Deploy container to AWS Lambda
* Implement query endpoint

## LLM Introduction
* My query function used the Pythia Scaling Suite model, which is a collection of models developed to facilitate interpretability research
* It contains two sets of eight models of sizes 70M, 160M, 410M, 1B, 1.4B, 2.8B, 6.9B, and 12B. For each size, there are two models: one trained on the Pile, and one trained on the Pile after the dataset has been globally deduplicated
* My function used the 1B-Q4 version in particular because it's small enough to run on lambda Link to Hugging Face model repo https://huggingface.co/rustformers/pythia-ggml

## Screenshot of AWS Lambda
![AWS_Lambda](/uploads/2cb6c51acd6452a035bc79eeb59b3fac/AWS_Lambda.png)

## Screenshot of cURL request against endpoint
* Query 1 - "What is Rust?"
![Query1](/uploads/285c29c5b7868faa9c4f65c45b5a41c2/Query1.png)
* Query 2 - "Where is Duke University?"
![Query2](/uploads/c02483cf6963509aa640d8136cbe793e/Query2.png)

## Key Steps
1. Make a new Cargo Lambda project by `cargo lambda new <proj name>`
2. Download a model that works with `rustformers` on Hugging Face (https://huggingface.co/rustformers)
3. Modify the `main.rs` file to include the model file and the query function
4. Modily `Cargo.toml` to include the necessary dependencies
5. Run `cargo lambda watch` to test query function locally
6. Run `curl http://localhost:9000 -X POST -H "Content-Type: application/json" -d '{"query": "What is the meaning of life?"}'` to test the query function
7. Go to the AWS portal and make sure you have `lambdafullaccess` and `iamfullaccess` in your IAM role
8. Get your `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` from the AWS portal and set them as environment variables
9. Go to ECR in AWS and create a new private repository
10. Click `View push commands` and follow the instructions to push your Docker image to the ECR
11. Once the image is pushed, go to Lambda and create a new function
12. Choose the container image option and select the image you just pushed
13. Set the memory to 3008 MB and the timeout to 15 minutes
14. Click `Deploy` and test the function by clicking `Test` and entering a query
15. Go to `Configuration` -> `Function URL`, create a new function URL, choose NONE for the security, and click `Deploy`
16. Copy the URL and test it with `curl` like the following: `curl -X POST <URL> -H "Content-Type: application/json" -d '{"query": "What is the meaning of life?"}'`

## References
* [OpenAI API Documentation](https://platform.openai.com/docs/overview)
* [Streamlit Documentation](https://docs.streamlit.io/)